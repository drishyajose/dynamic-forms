<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>



## Dynamic form
This is a project to create forms with different types for fields as per our choice.
Please follow below steps to run the project
<ol>
    <li>Pull the code</li>
<li>Configure database in .env</li>
<li>Run composer update</li>
<li>Run php artisan migrate</li>
<li>Run php artisan db:seed --class=UserSeeder</li>
<li>Login as admin, email : admin@admin.com, password : 12345678</li>
<li>Home page will show the Added forms if any</li>
<li>click add form to add new form</li>  
</ol>
